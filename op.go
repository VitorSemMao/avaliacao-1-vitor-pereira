package main

import "fmt"

var OpTable = map[string]Searchable {
	"sum": Soma{},
	"sub": subtracao{},
	"mul": multiplicacao{},
	"pow": potencia{},
	"root": raiz{},
}

func Run() {
	
	userInput := RequestUserInputAsString("operacao: ")
	
	if(!IsOpValid(userInput)) {
		fmt.Println("Invalid operation")
		return
	}
	
	rating, err := OpTable[userInput].CollectRatings()
	
	if(err != nil) {
		fmt.Println("Something was wrong")
		return
	}
	
	fmt.Printf("Rating: %v  Error: %v", rating, err)
	
	
}

func RequestUserInputAsString(message string) (string) {
	var userInput string
	fmt.Println(message)
	fmt.Scan(&userInput)
	return userInput
}

func IsOpValid(op string) (bool) {
	return OpTable[genre] != nil
}
