package main

import "math"

type pow struct {
	potencia
}

func (pow) calculo(num1, num2 float64) (float64, error) {
	y := math.Pow(num1, num2)
	return y, nil
}

